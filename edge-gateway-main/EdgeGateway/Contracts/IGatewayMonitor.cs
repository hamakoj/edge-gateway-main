﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hsrm.EdgeGateway.Contracts
{
    public interface IGatewayMonitor
    {
        void SendMonitoringMessage(GatewayMonitoringMessage message);
    }
}
