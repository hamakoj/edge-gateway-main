﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tecan.Sila2;
using Tecan.Sila2.DynamicClient;
using Tecan.Sila2.Server;

namespace Hsrm.EdgeGateway.Contracts
{
    internal interface IServerContextProvider
    {
        ServerContext Context { get; }
    }
}
