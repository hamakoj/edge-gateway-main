﻿using Hsrm.EdgeGateway.Contracts;
using Hsrm.EdgeGateway.GatewayService;
using Hsrm.EdgeGateway.ServiceDefinition;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tecan.Sila2;
using Tecan.Sila2.Client;
using Tecan.Sila2.Server;

namespace Hsrm.EdgeGateway
{
    [Export(typeof(IRequestInterceptor))]
    [Export(typeof(IServerGateway))]
    [Export(typeof(IServerResolver))]
    [Export(typeof(GatewayInterceptor))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    internal class GatewayInterceptor : IRequestInterceptor, IServerGateway, IServerContextProvider, IServerResolver
    {
        private readonly ConcurrentDictionary<Guid, ConnectedServer> _servers = new ConcurrentDictionary<Guid, ConnectedServer>();
        private readonly IExecutionManagerFactory _executionManagerFactory;

        public IEnumerable<ConnectedServer> ConnectedServers => _servers.Values;

        [ThreadStatic]
        private static ServerContext _serverContext;

        public GatewayInterceptor(IExecutionManagerFactory executionManagerFactory)
        {
            _executionManagerFactory = executionManagerFactory;
        }

        public int Priority => 10;

        public bool AppliesToCommands => true;

        public bool AppliesToProperties => true;

        public string MetadataIdentifier => "org.silastandard/gateway/GatewayService/v1/Metadata/ServerUuid";

        ServerContext IServerContextProvider.Context => _serverContext;

        public IRequestInterception Intercept(string commandIdentifier, ISiLAServer server, IMetadataRepository metadata)
        {
            try
            {
                var serverUuid = Guid.Parse(Metadata.Extract<StringDto>(metadata, MetadataIdentifier).Extract(server));
                var connectedServer = _servers[serverUuid];
                _serverContext = new ServerContext(connectedServer, metadata);
                return null;
            }
            catch (Exception ex)
            {
                throw server.ErrorHandling.CreateExecutionError("org.silastandard/gateway/GatewayService/v1/DefinedExecutionError/InvalidServerUuid", "The sent server identifier is not valid.", ex.Message);
            }
        }

        public bool IsInterceptRequired(Feature feature, string commandIdentifier)
        {
            return !commandIdentifier.StartsWith("org.silastandard/gateway")
                && !commandIdentifier.StartsWith("org.silastandard/core/SiLAService");
        }

        public void AddServer(ServerData server)
        {
            _servers.AddOrUpdate(server.Config.Uuid, id => CreateNewConnectedServer(server), (_, server) => server);
        }

        public bool RemoveServer(Guid serverUuid)
        {
            return _servers.TryRemove(serverUuid, out _);
        }

        private ConnectedServer CreateNewConnectedServer(ServerData server)
        {
            return new ConnectedServer(server, _executionManagerFactory.CreateExecutionManager(server));
        }

        public ConnectedServer ResolveServer(string serverUuid)
        {
            if (Guid.TryParse(serverUuid, out var serverId) && _servers.TryGetValue(serverId, out var connectedServer))
            {
                return connectedServer;
            }
            return null;
        }
    }
}
