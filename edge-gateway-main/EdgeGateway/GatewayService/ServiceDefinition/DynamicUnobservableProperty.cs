﻿using Hsrm.EdgeGateway.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tecan.Sila2;
using Tecan.Sila2.DynamicClient;

namespace Hsrm.EdgeGateway.ServiceDefinition
{
    internal class DynamicUnobservableProperty : DynamicServiceDefinition
    {
        private readonly Feature _featureDefinition;
        private readonly FeatureProperty _property;

        public DynamicUnobservableProperty(Feature featureDefinition, FeatureProperty property, IServerContextProvider contextProvider)
            : base(contextProvider)
        {
            _featureDefinition = featureDefinition;
            _property = property;
        }

        public DynamicObjectProperty Invoke()
        {
            var context = _contextProvider.Context;
            var client = new PropertyClient(_property, new FeatureContext(_featureDefinition, context.ConnectedServer.Server, context.ConnectedServer.ExecutionManager));
            return client.RequestValue(context.ConnectedServer.CreateAdditionalMetadata(context.Metadata));
        }
    }
}
