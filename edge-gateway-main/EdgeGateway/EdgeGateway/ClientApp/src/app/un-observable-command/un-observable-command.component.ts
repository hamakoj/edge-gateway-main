import { Component, OnInit } from '@angular/core';
import {CommandResult, UnobservableCommandService} from "../../../api";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-un-observable-command',
  templateUrl: './un-observable-command.component.html',
  styleUrls: ['./un-observable-command.component.css']
})
export class UnObservableCommandComponent implements OnInit {
  commandResult: CommandResult = {};
  commandResultForm!:FormGroup;

  constructor(private unobservableCommand: UnobservableCommandService) { }

  ngOnInit(): void {
    this.commandResultForm = new FormGroup({
      serverId: new FormControl('', [
        Validators.required
      ]),
      featureId: new FormControl('', [
        Validators.required
      ]),
      commandId: new FormControl('', [
        Validators.required
      ]),
      body: new FormControl('', [
        Validators.required
      ])
    });
  }

  get commandResultFormFormControl() {
    return this.commandResultForm.controls;
  }

  postUnobservableCommand(){
    const serverId = this.commandResultForm.value.serverId;
    const featureId = this.commandResultForm.value.featureId;
    const commandId = this.commandResultForm.value.commandId;
    const body:string = this.commandResultForm.value.body;

    this.unobservableCommand.apiExecutionServerIdFeatureIdCommandsCommandIdPost(serverId, featureId, commandId, body, "body", false, {httpHeaderAccept:"application/json"})
      .subscribe({
        next: (data) =>{
          this.commandResult = data;
          console.log(this.commandResult);
          return this.commandResult;

        },
        error:(error) => {
          console.log('Failed to post property:', error)
        },
      })

  }


}


