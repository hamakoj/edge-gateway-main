import { Component, OnInit } from '@angular/core';
import {ObservableCommandExecution, ObservableCommandService,} from "../../../api";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-observable-command',
  templateUrl: './observable-command.component.html',
  styleUrls: ['./observable-command.component.css']
})
export class ObservableCommandComponent implements OnInit {
  observableCommandExecution:ObservableCommandExecution = {};
  obsCmdExecForm!:FormGroup;

  constructor(private observableCommand: ObservableCommandService) { }

  ngOnInit(): void {
    this.obsCmdExecForm = new FormGroup({
      serverId: new FormControl('', [
        Validators.required
      ]),
      featureId: new FormControl('', [
        Validators.required
      ]),
      commandId: new FormControl('', [
        Validators.required
      ]),
      body: new FormControl('', [
        Validators.required
      ])
    });
  }
  get obsCmdExecFormFormControl() {
    return this.obsCmdExecForm.controls;
  }

  postObservableCommand(){
    const serverId = this.obsCmdExecForm.value.serverId;
    const featureId = this.obsCmdExecForm.value.featureId;
    const commandId = this.obsCmdExecForm.value.commandId;
    const body:string = this.obsCmdExecForm.value.body;

    this.observableCommand.apiExecutionServerIdFeatureIdObservablecommandsCommandIdPost(serverId, featureId, commandId, body, "body", false, {httpHeaderAccept:"application/json"})
      .subscribe({
        next: (data) =>{
          this.observableCommandExecution = data;
          console.log(this.observableCommandExecution);
          return this.observableCommandExecution;

        },
        error:(error) => {
          console.log('Failed to post property:', error)
        },
      })

  }

}
