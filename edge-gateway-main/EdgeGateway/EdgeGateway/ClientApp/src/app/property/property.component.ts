import { Component, OnInit } from '@angular/core';
import {DynamicObjectProperty, PropertyService} from "../../../api";
import {FormControl, FormGroup, Validators} from "@angular/forms";


@Component({
  selector: 'app-property',
  templateUrl: './property.component.html',
  styleUrls: ['./property.component.css']
})
export class PropertyComponent implements OnInit {
  dynamicObjectProperty:DynamicObjectProperty ={};
  propertyForm!:FormGroup;


  constructor(private propertyService: PropertyService) { }

  ngOnInit(): void {
    this.propertyForm = new FormGroup({
      serverId: new FormControl('', [
        Validators.required
      ]),
      featureId: new FormControl('', [
        Validators.required
      ]),
      propertyId: new FormControl('', [
        Validators.required
      ])
    });
  }

  get propertyFormFormControl() {
    return this.propertyForm.controls;
  }

  postProperty(){
    const serverId = this.propertyForm.value.serverId;
    const featureId = this.propertyForm.value.featureId;
    const propertyId = this.propertyForm.value.propertyId;

    this.propertyService.apiExecutionServerIdFeatureIdPropertiesPropertyIdPost(serverId,featureId,propertyId,'body', false, {httpHeaderAccept:"application/json"})
      .subscribe({
        next: (data) =>{
          this.dynamicObjectProperty = data;
      console.log(this.dynamicObjectProperty);
    },
      error:(error) => {
      console.log('Failed to post property:', error)
    },
      })

    }

}
