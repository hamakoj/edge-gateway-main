import {Component, ViewChild} from '@angular/core';
import {GatewayFeatureComponent} from "./gateway-feature/gateway-feature.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  @ViewChild(GatewayFeatureComponent) addFeature !:GatewayFeatureComponent;
  title = 'app';


}
