﻿using DryIoc;
using System.Text.Json;
using Tecan.Sila2;
using Tecan.Sila2.DynamicClient;

namespace Hsrm.EdgeGateway.Controllers
{
    public class CommandHelper
    {
        public static void FillRequest(DynamicRequest request, JsonDocument parameters)
        {
            FillRequest(o => request.Value = o, parameters.RootElement, request.Type);
        }

        private static void FillRequest(Action<object> target, JsonElement parameter, DataTypeType type)
        {
            switch (type.Item)
            {
                case BasicType basicType:
                    FillBasicType(target, parameter, basicType);
                    break;
                case ConstrainedType constrainedType:
                    FillRequest(target, parameter, constrainedType.DataType); 
                    break;
                case ListType listType:
                    FillList(target, parameter, listType.DataType);
                    break;
                case StructureType structureType:
                    FillStructure(target, parameter, structureType);
                    break;
            }
        }

        private static void FillStructure(Action<object> target, JsonElement parameter, StructureType structureType)
        {
            var obj = new DynamicObject();
            if (structureType.Element != null)
            {
                foreach (var elem in structureType.Element)
                {
                    var property = parameter.GetProperty(elem.Identifier);
                    var propertyValue = new DynamicObjectProperty(elem);
                    FillRequest(o => propertyValue.Value= o, property, elem.DataType);
                    obj.Elements.Add(propertyValue);
                }
            }
            target(obj);
        }

        private static void FillList(Action<object> target, JsonElement parameter, DataTypeType dataType)
        {
            var list = new List<object>();
            if (parameter.ValueKind == JsonValueKind.Array)
            {
                for (int i = 0; i < parameter.GetArrayLength(); i++)
                {
                    FillRequest(list.Add, parameter[i], dataType);
                }
            }
            else
            {
                FillRequest(list.Add, parameter, dataType);
            }
            target(list);
        }

        private static void FillBasicType(Action<object> target, JsonElement parameters, BasicType type)
        {
            switch (type)
            {
                case BasicType.String:
                    target(parameters.GetString() ?? "");
                    break;
                case BasicType.Integer:
                    target(parameters.GetInt64());
                    break;
                case BasicType.Real:
                    target(parameters.GetDouble());
                    break;
                case BasicType.Boolean:
                    target(parameters.GetBoolean());
                    break;
                case BasicType.Binary:
                    if (parameters.TryGetBytesFromBase64(out var bytes))
                    {
                        target(new MemoryStream(bytes));
                        break;
                    }
                    throw new NotSupportedException();
                case BasicType.Date:
                    target(parameters.GetDateTimeOffset());
                    break;
                case BasicType.Time:
                    target(parameters.GetDateTimeOffset());
                    break;
                case BasicType.Timestamp:
                    target(parameters.GetDateTimeOffset());
                    break;
                case BasicType.Any:
                    var anyObj = parameters.Deserialize<DynamicObjectProperty>();
                    if (anyObj != null)
                    {
                        target(anyObj);
                        break;
                    }
                    throw new NotSupportedException();
                default:
                    break;
            }
        }
    }
}
