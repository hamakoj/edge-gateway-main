﻿
using Hsrm.EdgeGateway.Contracts;
using Microsoft.AspNetCore.Mvc;
using Tecan.Sila2.Server.ConnectionConfiguration;

namespace Hsrm.EdgeGateway.Controllers
{
    [ApiController]
    [Route("/api/connectionConfiguration")]
    public class ConnectionConfigurationController : Controller
    {
        private readonly IConnectionConfigurationService _connectionConfigurationService;

        public ConnectionConfigurationController(IConnectionConfigurationService connectionConfigurationService)
        {
            _connectionConfigurationService = connectionConfigurationService;
        }

        [HttpGet]
        public IEnumerable<ConfiguredSiLAClient> GetConfiguredClients()
        {
            return _connectionConfigurationService.ConfiguredSiLAClients;
        }

        [HttpPut]
        public IActionResult AddConfiguredClient( [FromBody] ConfiguredSiLAClient client, bool persist = true)
        {
            if (!_connectionConfigurationService.ServerInitiatedConnectionModeStatus)
            {
                _connectionConfigurationService.EnableServerInitiatedConnectionMode();
            }
            _connectionConfigurationService.ConnectSiLAClient(client.ClientName, client.SiLAClientHost, client.SiLAClientPort, persist);
            return Ok();
        }

        [HttpDelete]
        public IActionResult RemoveConfiguredClient(string clientName, bool remove = true)
        {
            _connectionConfigurationService.DisconnectSiLAClient(clientName, remove);
            return Ok();
        }
    }
}
