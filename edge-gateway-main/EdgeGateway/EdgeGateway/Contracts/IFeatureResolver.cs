﻿using Tecan.Sila2;

namespace Hsrm.EdgeGateway.Contracts
{
    public interface IFeatureResolver
    {
        Feature? Resolve(string featureName);
    }
}
