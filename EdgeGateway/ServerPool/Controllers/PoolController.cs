﻿using Microsoft.AspNetCore.Mvc;
using Tecan.Sila2;
using Tecan.Sila2.Client;

namespace Hsrm.EdgeGateway.Controllers
{
    [ApiController]
    [Route("/api/gateway")]
    public class PoolController : Controller
    {
        private readonly IServerPool _serverPool;

        public PoolController(IServerPool serverPool)
        {
            _serverPool = serverPool;
        }

        [HttpGet]
        public IEnumerable<ServerInfo> GetConnectedServers()
        {
            return (_serverPool.ConnectedServers ?? Enumerable.Empty<ServerData>()).Select(s =>
                new ServerInfo(s.Config.Uuid.ToString(), s.Config.Name, s.Info.Type, s.Info.Description, s.Features.Select(f => f.FullyQualifiedIdentifier).ToArray()));
        }

        public record ServerInfo(
            string serveruuid,
            string serverName,
            string serverType,
            string description,
            IReadOnlyList<string> features);
    }
}
