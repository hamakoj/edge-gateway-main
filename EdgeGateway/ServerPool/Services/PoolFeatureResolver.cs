﻿using Hsrm.EdgeGateway.Contracts;
using Tecan.Sila2;
using Tecan.Sila2.Client;
using Tecan.Sila2.Server;

namespace Hsrm.EdgeGateway.Services
{
    public class PoolFeatureResolver : IFeatureResolver
    {
        private readonly IServerPool _serverPool;

        public PoolFeatureResolver(IServerPool serverPool)
        {
            _serverPool = serverPool;
        }

        public Feature? Resolve(string featureName)
        {
            var featuresWithIdentifier =
               (from server in _serverPool.ConnectedServers
                from feature in server.Features
                where string.Equals(feature.Identifier, featureName, StringComparison.InvariantCultureIgnoreCase)
                select feature).ToList();

            if (featuresWithIdentifier.Count == 1)
            {
                return featuresWithIdentifier[0];
            }

            var identifierConverted = featureName.Replace('_', '/');

            return (from server in _serverPool.ConnectedServers
                    from feature in server.Features
                    where string.Equals(feature.FullyQualifiedIdentifier, identifierConverted, StringComparison.InvariantCultureIgnoreCase)
                    select feature).FirstOrDefault();
        }
    }
}
