import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-daten-type',
  templateUrl: './daten-type.component.html',
  styleUrls: ['./daten-type.component.css']
})
export class DatenTypeComponent implements OnInit {

  value = [];
  bild = [];
  constructor() { }

  ngOnInit(): void {
    this.datenTypeValueId(this.value);
    this.getImagePath(this.bild);
  }


  datenTypeValueId(valueId: any): any {
    let typeofValue = typeof valueId;

    if (typeofValue === "string") {
      let isNum = /^\d+$/.test(valueId);
      if (isNum) {
        return parseInt(valueId);
      } else {
        let boolValue = valueId.toLowerCase();
        let isBoolean = /^(true|false)$/.test(boolValue);
        if (isBoolean) {
          return boolValue === 'true';
        } else {
          return valueId;
        }
      }
    } else if (typeofValue === "number" || typeofValue === "boolean") {
      return valueId;
    }
    return null;
  }

  getImagePath(valueId: any[]): string {
    console.log("bild", valueId);
    // Assuming you have two images named "true.jpg" and "false.jpg" in the assets folder
    return `assets/${'forest-gc25872c5d_1280'}.jpg`;
  }
}
