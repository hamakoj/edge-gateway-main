import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatenTypeComponent } from './daten-type.component';

describe('DatenTypeComponent', () => {
  let component: DatenTypeComponent;
  let fixture: ComponentFixture<DatenTypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DatenTypeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DatenTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
