import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {
  CommandResult,
  DiscoverySpec,
  DynamicObjectProperty,
  FeatureDesciption,
  FeatureService,
  GatewayFeatureService,
  GatewayService,
  ObservableCommandExecution,
  ObservableCommandService,
  PropertyService,
  ServerInfo,
  ServerSpec,
  UnobservableCommandService
} from "../../../api";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {catchError, forkJoin, Observable, tap, throwError} from 'rxjs';

declare const window: any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})

export class HomeComponent implements OnInit , OnDestroy{

  @ViewChild('boxRef', { static: false }) boxRef: ElementRef | undefined;

  serverInfoArray: ServerInfo[] = [];
  gatewayFeatures: Array<string> = [];
  dynamicObjectProperty: DynamicObjectProperty = {};
  featureDescription: FeatureDesciption = {};
  observableCommandExecution: ObservableCommandExecution | undefined  = {};
  UnObsCommandResult: CommandResult | undefined  = {};

  prop: DynamicObjectProperty = {};

  selectedFeature!: string;
  selectedServerUuid!: string;
  showTable = false;
  serviceName!: string;
  selectedStatus!: string;
  paramId!: string | null | undefined;
  valueId!:any ;
  parameterValues:string ='';

  commandId1!:string;
  ComParamIdResult: string ='';
  CommandResult: any;

  resultArray: any[] = [];

  serverForm!: FormGroup;
  numberForm!: FormGroup;
  propertyForm!: FormGroup;
  commandForm!:FormGroup;

  submitted = false;
  error = '';
  property!: string;
  text:string ='';
  errorMessage: string = "";
  previousCommandId:string = '';



  constructor(
    private gatewayService: GatewayService,
    private gatewayFeature: GatewayFeatureService,
    private observableCommand: ObservableCommandService,
    private unobservableCommand: UnobservableCommandService,
    private router: Router,
    private route: ActivatedRoute,
    private featureService: FeatureService,
    private propertyService: PropertyService,

    //private connection: signalR.HubConnection

) {}
  ngOnInit(): void {
    this.numberForm = new FormGroup({
      number: new FormControl('', [
        Validators.required,
        Validators.minLength(0),
        Validators.maxLength(100),
      ]),
    });

    this.serverForm = new FormGroup({
      host: new FormControl('', [Validators.required]),
      port: new FormControl('', [
        Validators.required,
        Validators.pattern('^[0-9]+'),
      ]),
    });

    this.propertyForm = new FormGroup({
      serverId: new FormControl('', [Validators.required]),
      featureId: new FormControl('', [Validators.required]),
      propertyId: new FormControl('', [Validators.required]),
    });

    this.commandForm = new FormGroup({
      serverId: new FormControl('', [
        Validators.required
      ]),
      featureId: new FormControl('', [
        Validators.required
      ]),
      commandId: new FormControl('', [
        Validators.required
      ]),
      body: new FormControl('', [
        Validators.required
      ])
    });


    let value = localStorage.getItem('yourDataKey');
    if (value){
      this.UnObsCommandResult = JSON.parse(value);
    }
    const savedServerInfoArray = localStorage.getItem('serverInfoArray');
    this.serverInfoArray = savedServerInfoArray ? JSON.parse(savedServerInfoArray) : [];

    this.postDiscoverySpec();
    this.getAllServerInfo()
    this.addServerSpec();
    this.postCommand(this.commandId1);

  }
  get numberFormControl() {
    return this.numberForm.controls;
  }
  get serverFormControl() {
    return this.serverForm.controls;
  }
  postDiscoverySpec() {
    let timeoutControl = this.numberFormControl['number'];
    if (timeoutControl && timeoutControl.valid) {
      let discoverySpec: DiscoverySpec = {
        timeout: this.numberFormControl['number'].value,
      };
      this.gatewayService
        .apiGatewayPost(discoverySpec, 'body', false)
        .subscribe({
          next: () => {
            console.log('send Successfully 1');
            alert('Time out send!!');
          },
          error: (err) => {
            this.error = err.error.message;
            alert('Form  Submitted Failed!!!');
          },
        });
      this.numberForm.reset();
    }
  }

  /** getAllServerInfo1() {
     this.postDiscoverySpec1();

    this.gatewayService.apiGatewayGet().subscribe((data) => {
      this.serverInfoArray = data;
      console.log('serverDetail', this.serverInfoArray);
    });

    this.gatewayFeature
     .apiGatewayFeaturesGet('body', false, { httpHeaderAccept: 'text/json' })
     .subscribe({
        next: (data) => {
          this.gatewayFeatures = data;
        },
        error: (error) => {
          console.error('Failed to get gateway feature:', error);
        },
      });

    localStorage.setItem('serverInfoArray', JSON.stringify(this.serverInfoArray));
  }**/
  getAllServerInfo() {
    this.postDiscoverySpec();

    forkJoin([
      this.gatewayService.apiGatewayGet(),
      this.gatewayFeature.apiGatewayFeaturesGet('body', false, { httpHeaderAccept: 'text/json' })
    ]).pipe(
      tap(([serverInfoArray, gatewayFeatures]) => {
        this.serverInfoArray = serverInfoArray;
        this.gatewayFeatures = gatewayFeatures;
        console.log('serverDetail', this.serverInfoArray);
        console.log('serverDetail', this.gatewayFeatures);
      }),
      catchError(error => {
        console.error('Failed to get server info:', error);
        // Handle the error or provide feedback to the user
        // For example, display an error message on the UI
        this.error = 'Failed to get server info';
        return throwError(error);
      })
    ).subscribe();
  }

  addServerSpec() {
    if (!this.serverForm.valid || !this.submitted) {
      console.log('Form not valid');
      return;
      }
    let serverSpec: ServerSpec = {
      host: this.serverForm.value.host,
      port: this.serverForm.value.port,
    };

      this.gatewayService.apiGatewayPut(serverSpec).subscribe({
        next:(response) =>{
          console.log('Successfully posted:',response);
        },error:(error) => {
          console.error('Failed to post:', error)
        }
      });
    }
  clearResultArray() {
    this.resultArray = [];
  }
  /**datenTypeValueId(valueId: any): any {
    let typeofValue = typeof valueId;
    if (typeofValue === "string") {
      let isNum = /^\d+$/.test(valueId);
      if (isNum) {
        return parseInt(valueId);
      } else {
        let boolValue = valueId.toLowerCase();
        let isBoolean = /^(true|false)$/.test(boolValue);
        if (isBoolean) {
          return boolValue === 'true';
        }else {
          // Check if the string contains a valid timezone format
          if (/[-+]\d{2}:\d{2}/.test(valueId)) {
            // Concatenate the timezone to the string
            return ;
          } else{
          return  valueId
        }
          }
      }
    } else if (typeofValue === "number" || typeofValue === "boolean") {
      return valueId;
    }
    return null;
  }**/
  datenTypeValueId(value: any): any {
    let typeofValue = typeof value;

    if (typeofValue === "string") {
      let isNum = /^\d+$/.test(value);
      if (isNum) {
        return parseInt(value, 10); // Specify the radix to avoid potential issues
      } else {
        let boolValue = value.toLowerCase();
        let isBoolean = /^(true|false)$/.test(boolValue);
        if (isBoolean) {
          return boolValue === 'true';
        } else {
          const timezoneRegex = /([-+]\d{2}:\d{2})$/;
          let hasTimezone = timezoneRegex.test(value); // Changed the variable name for clarity

          if (hasTimezone) {
            // Extract the timezone and concatenate it to the string
            // @ts-ignore
            const timezone = timezoneRegex.exec(valueId)[1];
            return value.replace(timezoneRegex, '') + ' ' + timezone;
          } else {
            return value;
          }
        }
      }
    } else if (typeofValue === "number" || typeofValue === "boolean") {
      return value;
    }

    return null;
  }

  getBody1(parameterId1: any, box: HTMLInputElement) {
    if (parameterId1 !== this.datenTypeValueId(box.value)) {
      console.log(' datenTypes value',box.value)
      this.errorMessage = `Input value does not match the parameter identifier for ${parameterId1}.`;
    }
    if(parameterId1 != null){
      this.parameterValues = parameterId1;
      this.valueId = this.datenTypeValueId(box.value);
      console.log('value id', this.valueId);
    }
  }
  isSameType(value: any): boolean {
    return typeof value === typeof this.valueId;
  }
  property12( featureid: any, propertyid: any):Observable<any> {
    let serverId = this.propertyForm.value.serverId;
    let featureId = featureid;
    if (featureId) {
      const parts = featureId.split('/');
      featureId = parts[parts.length - 2];
    }
    this.showTable= true;
    return this.propertyService
      .apiExecutionServerIdFeatureIdPropertiesPropertyIdPost(serverId, featureId, propertyid, 'body', false, { httpHeaderAccept: 'application/json' })
      .pipe(tap(
        (data) => {
          this.dynamicObjectProperty = data;
        })
      );
  }
  extractServiceName(selectedFeature: string) {
    const parts = selectedFeature.split('/');
    this.serviceName = parts[parts.length - 2];

    this.featureService
      .apiFeatureIdentifierGet(this.serviceName, 'body', false)
      .subscribe((response) => {
        this.featureDescription = response;
        console.log('feature description:', this.featureDescription)
        const featureIdentifier = this.featureDescription.identifier;
        if(this.featureDescription.properties != null){
          this.clearResultArray();
          for (let propIdentifier of this.featureDescription.properties) {
            this.property12(featureIdentifier , propIdentifier.identifier).subscribe(
              (result)=>{
               this.prop = result;
                this.resultArray.push(result);
              });
          }}});
  }

  postCommand(CommandId1: any) {
    if (this.featureDescription.commands) {
      let serverId = this.commandForm.value.serverId;
      let featureId = this.serviceName;
      let commandId = CommandId1;
      let body = {};

      const validatedValueId = this.valueId;

      body = { [this.parameterValues]: validatedValueId };

      if (validatedValueId !== null) {
        console.log('validatedValue1', validatedValueId);

        /** const connection = new HubConnectionBuilder()
         .withUrl('https://localhost:44408/execution')
         .configureLogging(LogLevel.Information)
         .build();

         connection.start()
         .then(() => {
            connection.invoke('ExecuteCommand', serverId, featureId, commandId, body)
              .then(() => {
                console.log('Command executed successfully.');
              })
              .catch((error) => {
                console.log('Failed to execute command:', error);
              })
              .finally(() => {
                connection.stop();
              });
          })
         .catch((error) => {
            console.log('Failed to start SignalR connection:', error);
          });**/

        if (this.featureDescription.commands[0].observable === 1) {
          this.unobservableCommand.apiExecutionServerIdFeatureIdCommandsCommandIdPost(serverId, featureId, commandId, body, "body", false, { httpHeaderAccept: "application/json" })
            .subscribe({
              next: (data) => {
                this.UnObsCommandResult = data;
                console.log('unobservable command:', this.UnObsCommandResult);
                for (let result of this.UnObsCommandResult.result?.value.elements) {
                  this.ComParamIdResult = result.value;
                  console.log('result', this.ComParamIdResult);
                }
              },
              error: (error) => {
                console.log('Failed to post property 1:', error);
              },
            });
          this.ComParamIdResult= '';
          localStorage.setItem('yourDataKey', JSON.stringify(this.UnObsCommandResult));
        }
        else {
          this.observableCommand.apiExecutionServerIdFeatureIdObservablecommandsCommandIdPost(serverId, featureId, commandId, body, "body", false, { httpHeaderAccept: "application/json" })
            .subscribe({
              next: (data) => {
                this.observableCommandExecution = data;
                console.log('observable command:', this.observableCommandExecution);
                if (this.CommandResult) {
                  this.CommandResult = this.observableCommandExecution.executionUUID;
                }
              },
              error: (error) => {
                console.log('Failed to post property 2:', error);
              }
            });
          /**  connection.on('CommandExecution', (data) => {
            console.log('signal', data);
          });**/
        }
        // Update previous command and parameter IDs
        this.previousCommandId = CommandId1;
        if (this.boxRef) {
          this.boxRef.nativeElement.value = ''; // Reset the input value to an empty string
        }
        this.parameterValues = '';
        this.valueId ='';

      }
    }
  }

  clearInput(inputRef: ElementRef | undefined) {
    inputRef!.nativeElement.value = '';
  }
  ngOnDestroy(): void {
      window.localStorage.removeItem('yourDataKey');
  }
}
