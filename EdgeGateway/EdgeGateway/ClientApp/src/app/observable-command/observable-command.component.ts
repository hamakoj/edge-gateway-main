import { Component, OnInit } from '@angular/core';
import {
  CommandResult,
  FeatureDesciption,
  FeatureService,
  GatewayFeatureService,
  GatewayService,
  ObservableCommandExecution,
  ObservableCommandService,
  ServerInfo,
  UnobservableCommandService,
} from '../../../api';
import { FormControl, FormGroup, Validators } from '@angular/forms';




@Component({
  selector: 'app-observable-command',
  templateUrl: './observable-command.component.html',
  styleUrls: ['./observable-command.component.css'],
})
export class ObservableCommandComponent implements OnInit {
  observableCommandExecution: ObservableCommandExecution = {};
  commandResult: CommandResult = {};
  commandForm!:FormGroup;


  serviceName!: string;
  selectedServerUuid!: string;
  selectedFeature!: string;
  paramId!: string | null | undefined;
  valueId: string = '';


  featureDescription: FeatureDesciption = {};
  serverInfoArray: ServerInfo[] = [];
  gatewayFeatures: string[] = [];


  constructor(
    private observableCommand: ObservableCommandService,
    private unobservableCommand: UnobservableCommandService,
    private featureService: FeatureService,
    private gatewayService: GatewayService,
    private gatewayFeature: GatewayFeatureService,

  ) {}

  ngOnInit(): void {
    this.commandForm = new FormGroup({
      serverId: new FormControl('', [
        Validators.required
      ]),
      featureId: new FormControl('', [
        Validators.required
      ]),
      commandId: new FormControl('', [
        Validators.required
      ]),
      body: new FormControl('', [
        Validators.required
      ])
    });

    //this.getAllServerInfo1();
    //this.postCommand(this.valueId);
  }

  /**
   * getAllServerInfo1() {
    this.gatewayService.apiGatewayGet().subscribe((data) => {
      this.serverInfoArray = data;
      console.log('serverDetail', this.serverInfoArray);
    });
    this.gatewayFeature
      .apiGatewayFeaturesGet('body', false, { httpHeaderAccept: 'text/json' })
      .subscribe({
        next: (data) => {
          this.gatewayFeatures = data;
          console.log('Gateway feature:' , this.gatewayFeatures);
        },
        error: (error) => {
          console.error('Failed to get gateway feature:', error);
          // Handle the error appropriately (e.g., show an error message)
        },
      });
  }
   * **/

  /**
   * extractServiceName(selectedFeature: string) {
    const parts = selectedFeature.split('/');
    this.serviceName = parts[parts.length - 2];
    console.log('extract name:', this.serviceName);

    this.featureService
      .apiFeatureIdentifierGet(this.serviceName, 'body', false)
      .subscribe((response) => {
        console.log('resp:', response);
        this.featureDescription = response;
        console.log('show command:', this.featureDescription.commands);
      });
    this.serviceName = '';
  }
   * **/

  /**
   * getbody(body: any) {
    this.valueId = body;
    console.log('write body',body);
  }
   * **/

  /**
   * postCommand(parameterIdentifier: any ){
    if(this.featureDescription.commands){
      for(let paramId of this.featureDescription.commands){
        let id = paramId.identifier;
        console.log('parIdent',id)
      }
    }
   // const paramId = this.featureDescription.commands;
    //console.log('paramId', paramId);

    let serverId = this.commandForm.value.serverId;
    let featureId = this.commandForm.value.featureId;
    let commandId = parameterIdentifier;
    // let body:string = this.commandForm.value.body;

    let body =  JSON.parse(`{\"StringValue\":\"text\"}`);

    console.log('body1', body);
    console.log('params',parameterIdentifier);
    if (this.featureDescription.commands && this.featureDescription.commands[0].observable === 1){

      this.unobservableCommand.apiExecutionServerIdFeatureIdCommandsCommandIdPost(serverId, featureId, commandId, body , "body", false, {httpHeaderAccept:"application/json"})
        .subscribe({
          next: (data) =>{
            this.commandResult = data;
            console.log('unobservable command:', this.commandResult);
          },
          error:(error) => {
            console.log('Failed to post property 1:', error)
          },
        });
    }
    else{
      this.observableCommand.apiExecutionServerIdFeatureIdObservablecommandsCommandIdPost(serverId, featureId, commandId, body , "body", false, {httpHeaderAccept:"application/json"})
        .subscribe({
          next: (data) =>{
            this.observableCommandExecution = data;
            console.log('observable command:', this.observableCommandExecution);
          },
          error:(error) => {
            console.log('Failed to post property 2:', error)
          },
        });
    }
  }
   * **/




}
