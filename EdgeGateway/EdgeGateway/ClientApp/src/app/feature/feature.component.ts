import { Component, OnInit } from '@angular/core';
import {FeatureDesciption, FeatureService} from "../../../api";

@Component({
  selector: 'app-feature',
  templateUrl: './feature.component.html',
  styleUrls: ['./feature.component.css']
})
export class FeatureComponent implements OnInit {
  featureDescription: FeatureDesciption = {};
  f_identifier: string = '';

  constructor(private featureService: FeatureService) { }

  ngOnInit(): void {
    this.getFeature();
  }

  getFeature() {
    console.log('r2:');

    this.featureService
      .apiFeatureIdentifierGet(this.f_identifier, 'body', false)
      .subscribe((response) => {
        console.log('resp:',response);
        this.featureDescription = response;
        console.log(this.featureDescription.identifier)
      });
    this.f_identifier= '';
  }

}
