﻿using Hsrm.EdgeGateway.Contracts;
using Microsoft.AspNetCore.Mvc;
using Tecan.Sila2;

namespace Hsrm.EdgeGateway.Controllers
{
    [ApiController]
    [Route("/api/gatewayFeatures")]
    public class GatewayFeatureController : Controller
    {
        private static readonly string _featuresDir = Path.Combine(Environment.CurrentDirectory, "Features");
        private readonly ILogger<GatewayFeatureController> _logger;
        private readonly IFeatureResolver _featureResolver;

        public GatewayFeatureController(ILogger<GatewayFeatureController> logger, IFeatureResolver featureResolver)
        {
            _logger = logger;
            _featureResolver = featureResolver;
        }

        [HttpGet]
        public IEnumerable<string> GetGatewayFeatures()
        {
            var features = new List<string>();
            foreach (var file in Directory.GetFiles(_featuresDir))
            {
                try
                {
                    var feature = FeatureSerializer.Load(file);
                    features.Add(feature.FullyQualifiedIdentifier);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"Error reading {file}");
                }
            }
            return features;
        }

        [HttpPut]
        public IActionResult PutFeature(string identifier)
        {
            var feature = _featureResolver.Resolve(identifier);
            if (feature == null)
            {
                return NotFound();
            }
            var featurePath = GetFileName(feature);
            if (!System.IO.File.Exists(featurePath))
            {
                FeatureSerializer.Save(feature, featurePath);
                return Ok();
            }
            return Conflict();
        }

        private static string GetFileName(Feature feature)
        {
            return Path.Combine(_featuresDir, feature.FullyQualifiedIdentifier.Replace('/', '.') + ".xml");
        }

        [HttpDelete]
        public IActionResult DeleteFeature(string identifier)
        {
            var feature = _featureResolver.Resolve(identifier);
            if (feature == null)
            {
                return NotFound();
            }
            var featurePath = GetFileName(feature);
            if (System.IO.File.Exists(featurePath))
            {
                System.IO.File.Delete(featurePath);
                return Ok();
            }
            return NotFound();

        }
    }
}
