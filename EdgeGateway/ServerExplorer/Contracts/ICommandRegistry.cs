﻿using Tecan.Sila2.DynamicClient;

namespace Hsrm.EdgeGateway.Contracts
{
    public interface ICommandRegistry
    {
        void RegisterCommand(DynamicCommand commandExecution);
    }
}
