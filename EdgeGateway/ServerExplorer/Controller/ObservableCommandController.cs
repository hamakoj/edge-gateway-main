﻿using Hsrm.EdgeGateway.Contracts;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using Tecan.Sila2.DynamicClient;
using Tecan.Sila2;

namespace Hsrm.EdgeGateway.Controllers
{
    [ApiController]
    [Route("/api/execution/{serverId}/{featureId}/observablecommands/{commandId}")]
    public class ObservableCommandController : Controller
    {
        private readonly IFeatureResolver _featureResolver;
        private readonly IServerResolver _serverResolver;
        private readonly ICommandRegistry _commandRegistry;

        public ObservableCommandController(IFeatureResolver featureResolver, IServerResolver serverResolver, ICommandRegistry commandRegistry)
        {
            _featureResolver = featureResolver;
            _serverResolver = serverResolver;
            _commandRegistry = commandRegistry;
        }

        [HttpPost]
        public ActionResult<ObservableCommandExecution> Invoke(string serverId, string featureId, string commandId, [FromBody] JsonDocument parameters)
        {
            var server = _serverResolver.ResolveServer(serverId);
            var feature = _featureResolver.Resolve(featureId);

            var command = feature.Items.OfType<FeatureCommand>()
                .FirstOrDefault(c => string.Equals(c.Identifier, commandId, StringComparison.OrdinalIgnoreCase));

            if (command == null)
            {
                return BadRequest("Command not found");
            }
            if (command.Observable == FeatureCommandObservable.No)
            {
                return BadRequest("Command is not observable");
            }

            var commandClient = new ObservableCommandClient(command, new FeatureContext(feature, server.Server, server.ExecutionManager));
            var request = commandClient.CreateRequest();
            try
            {
                CommandHelper.FillRequest(request, parameters);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            try
            {
                var response = commandClient.Invoke(request);
                _commandRegistry.RegisterCommand(response);
                return new ObservableCommandExecution(response.CommandId.CommandId, null);
            }
            catch (DefinedErrorException definedError)
            {
                return new ObservableCommandExecution(null, definedError.Message);
            }
            catch (Exception silaException)
            {
                return new ObservableCommandExecution(null, silaException.Message);
            }
        }
    }
}
