﻿using Hsrm.EdgeGateway.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tecan.Sila2.Client;

namespace Gateway.ServerPool
{
    public static class ServerPoolConnector
    {
        public static void Connect(IServerPool serverPool, IServerGateway gateway)
        {
            foreach (var connectedServer in serverPool.ConnectedServers)
            {
                gateway.AddServer(connectedServer);
            }

            serverPool.ServerConnected += (o, e) => gateway.AddServer(e.Server);
        }
    }
}
