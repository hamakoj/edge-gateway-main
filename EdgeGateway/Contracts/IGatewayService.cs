﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tecan.Sila2;
using Tecan.Sila2.Server;
using MetadataTypeAttribute = Tecan.Sila2.Server.MetadataTypeAttribute;

namespace Hsrm.EdgeGateway.Contracts
{
    /// <summary>
    /// This feature is used to provide generic gateway functionality
    /// </summary>
    [SilaFeature(true, "gateway")]
    public interface IGatewayService
    {
        /// <summary>
        /// This client metadata needs to be attached to every command invocation in order to specify the server on which the command must be executed
        /// </summary>
        [MetadataType(typeof(string))]
        IRequestInterceptor ServerUuid { get; }

        /// <summary>
        /// Lists the servers connected to this gateway
        /// </summary>
        [Observable]
        IReadOnlyList<Server> ConnectedServers { get; }

        /// <summary>
        /// Performs a network discovery for the specified duration
        /// </summary>
        /// <param name="duration">The duration for which the gateway should look for servers</param>
        void ScanNetwork([Unit("s", Second = 1)] int duration);

        /// <summary>
        /// Adds the server with the given host name and port
        /// </summary>
        /// <param name="host">The host name of the server</param>
        /// <param name="port">The port on which the server services requests</param>
        void AddServer(string host, int port);
    }
}
