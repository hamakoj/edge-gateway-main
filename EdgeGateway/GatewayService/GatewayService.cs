﻿using Hsrm.EdgeGateway.Contracts;
using Hsrm.EdgeGateway.ServiceDefinition;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tecan.Sila2;
using Tecan.Sila2.Client;
using Tecan.Sila2.Discovery;
using Tecan.Sila2.Server;

namespace Hsrm.EdgeGateway.GatewayService
{
    [Export(typeof(IGatewayService))]
    [Export(typeof(IGatewayConfigurationService))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    internal class GatewayService : IGatewayService, INotifyPropertyChanged, IGatewayConfigurationService
    {
        public IRequestInterceptor ServerUuid => _interceptor;

        public IReadOnlyList<Server> ConnectedServers
        {
            get;
            private set;
        }

        private readonly ConcurrentDictionary<string, DynamicFeatureProvider> _features = new ConcurrentDictionary<string, DynamicFeatureProvider>();
        private readonly GatewayInterceptor _interceptor;
        private readonly IServerDiscovery _serverDiscovery;
        private readonly IServerConnector _connector;
        private readonly ISiLAServer _server;
        private readonly ISiLAService _silaService;

        [ImportingConstructor]
        public GatewayService(GatewayInterceptor interceptor, IServerDiscovery serverDiscovery, IServerConnector connector, ISiLAServer server, ISiLAService silaService)
        {
            _interceptor = interceptor;
            _serverDiscovery = serverDiscovery;
            _connector = connector;
            _server = server;
            _silaService = silaService;

            var dir = Path.GetDirectoryName(typeof(GatewayService).Assembly.Location);
            var features = Path.Combine(dir, "Features");
            if (Directory.Exists(features))
            {
                foreach (var featureFile in Directory.EnumerateFiles(features, "*.sila.xml"))
                {
                    var feature = FeatureSerializer.Load(featureFile);
                    AddFeature(feature);
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void ScanNetwork([Unit("s", Second = 1)] int duration)
        {
            _serverDiscovery.DiscoverServers(AddServerCore, TimeSpan.FromSeconds(duration), nic => true);
        }

        private void AddFeature(Feature feature)
        {
            var id = feature.FullyQualifiedIdentifier;
            var provider = new DynamicFeatureProvider(feature, _interceptor, _server);
            if (_features.TryAdd(id, provider) && !_silaService.ImplementedFeatures.Contains(id))
            {
                _server.AddFeature(provider);
            }
        }

        public void AddServer(string host, int port)
        {
            var server = _connector.Connect(host, port);

            AddServerCore(server);
        }

        private void AddServerCore(ServerData server)
        {
            if (server.Config.Uuid.ToString() != _silaService.ServerUUID)
            {
                _interceptor.AddServer(server);

                foreach (var feature in server.Features)
                {
                    AddFeature(feature);
                }

                ConnectedServers = _interceptor.ConnectedServers.Select(s => s.Info).ToList();
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(ConnectedServers)));
            }
        }

        public void RemoveServer(string serverUuid)
        {
            if (!Guid.TryParse(serverUuid, out var serverGuid) || !_interceptor.RemoveServer(serverGuid))
            {
                throw new ArgumentOutOfRangeException(nameof(serverUuid), $"No server with Id {serverUuid} is connected");
            }
        }
    }
}
