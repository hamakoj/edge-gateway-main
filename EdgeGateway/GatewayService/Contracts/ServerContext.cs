﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tecan.Sila2.Server;
using Tecan.Sila2;
using Tecan.Sila2.DynamicClient;
using Tecan.Sila2.Client;
using Hsrm.EdgeGateway.GatewayService;

namespace Hsrm.EdgeGateway.Contracts
{
    internal class ServerContext
    {
        public ServerContext(ConnectedServer connectedServer, IMetadataRepository metadata)
        {
            ConnectedServer = connectedServer;
            Metadata = metadata;
        }

        public ConnectedServer ConnectedServer { get; }

        public IMetadataRepository Metadata { get; }
    }
}
